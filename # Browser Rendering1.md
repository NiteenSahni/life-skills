# Browser Rendering
The browser sends a request to a server to fetch an HTML document, and the server returns an HTML page in binary stream format as a text file with the response header content-type set to the value text/html; charset=UTF-8.

* Text/html tells the browser that the file is an HTML document.
* Charset=UTF-8 tells the browser that the file is encoded in UTF-8 binary format.
  * Using this information the browser converts the binary file into a readable text file.
## DOM (Document Object Model)

Whenever the browser reads the HTML code and encounters an HTML element eg. (div, body, IMG, etc..), it converts that element into a javascript object called Node, and then into individual JavaScript Objects.
Now the browser creates a tree-like structure consisting of all these Nodes, This is called a DOM Tree.

![DOM Tree](./images/domtree.jpg)

After the DOM tree has been created, the browser reads CSS from all the sources (external, embedded, inline, user-agent, etc.) and constructs a CSSOM.

CSSOM stands for CSS Object Model which is a Tree Like structure to a DOM tree.

## CSSOM
CSSOM is a tree-like structure similar to a DOM tree, it differs from the DOM tree in the way that it only contains info about the things that are going to be printed on the screen.

![CSSOM Tree](./images/CSSOMtree.jpg)

## Render Tree

Render-Tree is also a tree-like structure, similar to a DOM tree ad CSSOM tree, it is constructed by combining DOM and CSSOM trees. The browser has to calculate the layout of each visible element and paint them on the screen, for that browser uses this Render-Tree. So we need both the DOM tree and the CSS tree for something to get printed on the screen.

![Render Tree](./images/rendertree.jpg)


## Rendering Sequence
After the browser is done with the completion of creating the DOM, CSSOM and Render trees, it starts with the printing of individual elements on the screen.

## Layout Operation 
The browser first creates the layout of each individual Render tree Node,
which consists of the size of each node pixel and its position on the screen.
## Painting
Painting means rendering the pixels and applying stylistic properties.
## Compositing Operation 
After the Layout operation we have the info about the different layers that need to be drawn on the screen and the order it should follow, now this info is sent to the GPU to finally draw the pixels on the screen.
* Image shows us the Order of these operations.
 
 ![Render Tree](./images/order.png)

### Browser creates DOM, CSSOM tree and then creates the Render tree and then goes through with the different processes that are Layout, Paint, and Compositing to render a webpage.

## Browser Engine
The Browser rendering engine is single-threaded, excluding network operations, everything happens in this thread.
Browser engine handles the job of creating the DOM, CSSOM, and the Render Tree, Browser engines are also known as Rendering Engine or Layout Engine.
Browser engine contains the logic which is required to render the webpage from the HTML file.
## Working of Browser Engine
The Browser Engine goes through the following process to ultimately render a webpage.
### Parsing 
Parsing is the process in which the browser reads the HTML document and constructs a DOM tree from it.Hence the process is also called DOM parsing and the program that does that is called the DOM parser.

The browser parses the HTML document as soon as a few characters are loaded, so it builds the DOM tree incrementally one Node at a time.
![Parse](./images/parse.webp)


Now, when the browser encounters a script file via (script) element or a stylesheet via (link) element the browser starts the download in the background, which is away from the main thread of the JavaScript execution.
* Dom parsing happens on the main thread, So if the main JavaScript execution thread is busy, DOM parsing will not progress until the thread is free.
* script elements are parser blocking.
### Parser-Blocking Scripts
A parser-blocking script is any piece of code that stops the parsing of the HTML, the script will get executed first and then the parsing will continue.

![parserBlockingScript](./images/parserblocking.jpg)

* If the script is external, then the browser will start the download of the script and stop the execution of the main thread until the file is downloaded.
* If the script tag has the async attribute, the browser engine does not halt the parsing process while the file is being downloaded in the background, but once the file is downloaded the parsing will halt and the script code will be executed first.

![defer](./images/defer.jpg)

* Browser engine also has a defer attribute, in this case, the defer script is executed after parsing the HTML file that is after the creation of the DOM tree.

![Async](./images/async.jpg)


### Render-Blocking CSS
 Now, the DOM tree is constructed in an incremental manner whereas the CSSOM tree is constructed in a specific manner.
 When the browser finds the (style) block, it will parse all the embedded CSS and update the CSSOM tree with new CSS (style) rules. After that, the HTML parsing continues in a normal manner.
 
 CSS is a render blocking resource, as soon as the browser makes a request to fetch an external stylesheet the render tree construction is halted, during this process nothing gets rendered on the screen.

![parserBlockingCss](./images/parserblockingcss.jpg)
 ### Document’s DOMContentLoaded Event
 * The DOMContentLoaded (DCL) event marks a point in time when the browser has constructed a complete DOM tree from all the available HTML. 

 * If our script does not contains any scripts, DOM parsing doesnt gets blocked and the DCL fires as soon as the browser parses the HTML document.

 # Refrences
 * [Speculative Parsing](https://hacks.mozilla.org/2017/09/building-the-dom-faster-speculative-parsing-async-defer-and-preload/)
 * [StackOverflow](https://stackoverflow.com/)
 * [BrowserRendering](https://medium.com/jspoint/how-the-browser-renders-a-web-page-dom-cssom-and-rendering-df10531c9969)
