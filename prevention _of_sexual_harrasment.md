# Prevention of Sexual Harassment

### Q1.) What kinds of behaviour cause sexual harassment?
Ans.) 
* Sexual harassment is any unwelcome verbal visual or physical conduct  of a sexual nature that is severe or pervasive and affects the working conditions of the employees or creates a hostile work environment. 
* There are generally three forms of sexual harassment behavior verbal visual and physical verbal harassment includes comments about clothing or a person's body sexual or gender-based jokes or remarks requesting sexual favors or repeatedly asking the person out it also includes sexual innuendos threats spreading rumors about a person's personal or sexual life or using foul and obscene language visual harassment can include posters drawings pictures screen savers cartoons emails or texts of a sexual nature physical harassment often includes sexual assault impeding or blocking movement inappropriate touching such as kissing hugging patting stroking or rubbing sexual gesturing or even leering or staring.


### Q2. ) What would you do in case you face or witness any incident or repeated incidents of such behaviour?
Ans.)

 While each person needs to decide what action plan works best for him or herself, studies have found that informal actions result from the fastest solutions. One should directly confront the person involved to stop the unacceptable behavior, if this does not work, one should put this in writing. If one witnesses any sexual harassment, one should offer support to the victim and put a written complaint to the higher authorities.
### Steps to be followed if one witnesses sexual harassment at the workplace
* Write a complaint to the authorities.
* Record the dates on which you take any steps to try and address the problem.
* Make a copy of any written reports or complaints that you decide to file.
* Offer support to the victim.