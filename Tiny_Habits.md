#  Tiny Habits 
## Question 1

Your takeaways from the video (Minimum 5 points).
## Answer

The video talks about how one can develop tiny habits, he described some key points to be followed, that are:-

* One should not look for motivation to make something a habit but do something simple and then build it up.

* The video gives the 3 elements of behavior that is (MAP).
* 'M' refers to Motivtion.
* 'A' refers to Ability.
* 'P' refers to Prompt, which is the cue to do something.
* The video says that tiny habits are effective because:-

    
    * They’re fast to do, can fit into any schedule, and allows you to start now.
    * They’re safe, unlike big or risky actions. 
    * If you make a mistake, it’s easy to try again.
    * They’re so tiny that you don’t need to rely on your limited willpower.
    * Yet, they add up over time to deliver dramatic changes.
    

* The video finally talks about pairing a tiny habit with an already existing one, so that the existing habit serves as a cue to the new habit.

## Question 2

Your takeaways from the video in as much detail as possible

## Answer

The video further breaks downs the points discussed in the previous video. The video explains how BJ gave the (B=MAP) behavior model, where he explained that behavior change is possible not only by motivation but can also be achieved by creating or breaking down a change to its most minute action and making that a habit. Now he explained why this method works in the following points:-  
* They’re fast to do, can fit into any schedule, and allows you to start now.
* They’re safe, unlike big or risky actions. 
* If you make a mistake, it’s easy to try again.
 * They’re so tiny that you don’t need to rely on your limited willpower.
* Yet, they add up over time to deliver dramatic changes.

He further goes on to explain that one needs an action prompt to successfully create a new habit, which means pairing a habit with a pre-existing habit so that the momentum from the previous action helps you do the little task that will form a new habit.

## Question 3

How can you use B = MAP to make making new habits easier?

## Answer

One can take the following steps to make a new habit easier:-

* Break the change into little tiny habits.
* Pair the habits with an action prompt.
* Celebrate after small victories.

## Question 4

Why it is important to "Shine" or Celebrate after each successful completion of a habit?

## Answer

It's important to celebrate after the completion of a habit because it makes you feel that you have accomplished something and that psychologically serves as a reward for the habit.

## Question 5

Your takeaways from the video (Minimum 5 points)

## Answer

The video talks about how to cultivate a habit. It breaks the process into some points, some of which are Noticing Wanting Doing Liking.
Some of the points shared in the video are:-

* Time plays an important role in cumulating your habits, good habits or bad habits compound over time.

* Give your goals a time and place in the world, and set a fixed time to execute said tasks.
* Create a backup plan for your habits if you miss them you have a backup time to execute them.
* Design your environment to support your habit.
* Place steps between you and a behavioral pattern you want to avoid.
* Plan to optimize the start not the finish.
* Don't break the chain of habits, if you ever make a mistake and miss one day, make sure to never miss twice.

## Question 6

Write about the book's perspective on habit formation from the lens of Identity, processes, and outcomes.

## Answer 
The book tells you to change your identity which will eventually lead to a change in habit formation.
The processes discussed are:-
* Don't care about the outcome, but invest in building a sustainable journey, or make a plan that helps you continue playing the game.
The outcome of all the processes mentioned in the book will eventually lead to one cultivation of a habit and becoming good at it.

## Question 7

Write about the book's perspective on how to make a good habit easier.

## Answer

The book starts with the topic of compounding habits with time and time is an ally when the habit is good and an enemy when the habit is bad. The book discusses the plateau of disappointment which is the disappointment one feels when he feels the progress planned is not being achieved, which is a normal thing as progress does not have a linear relationship with time. Then the book brings up the point of sustainability,
as to plan the journey or build the system to continue playing the game, as setting goals can be pointless if the goals are too hard to achieve one would lose the motivation, or if the goal is reached one would stop trying. The book then raises the point that identity change is the road to habit change. In the end, the book gives various methods to create a new habit sustainable some of which are:-

* Break the change into little tiny habits.
* Pair the habits with an action prompt.
* Celebrate after small victories.

## Question 8

Write about the book's perspective on making a bad habit more difficult.

## Answer

The book advises to create steps or increase the number of steps between you and the habit you want to change or quit.

## Question 9:

Pick one habit that you would like to do more of. What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?

## Answer
I would like to solve at least one google interview question every day and make this a habit. I can pair this habit with my work as an activity that I would end the day with. The end of work will serve as a cue to the habit. I can treat myself with candy as a reward for achieving my goal.

##  Question 10:

Pick one habit that you would like to eliminate or do less of. What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?

## Answer

The one habit I would like to eliminate is overthinking, I can make overthinking hard by applying certain philosophies that tell me that overthinking is pointless.
