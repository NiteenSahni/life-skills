# Focus Management

## 1. What is Deep Work

## Answer 
The video describes deep work as working without getting distractions.

## Question 2

Paraphrase all the ideas in the above videos and this one in detail.

## Answer

* The first video talks about the optimal duration of deep work, and how long you should work without taking a break, the video describes that the minimum duration of deep work should be 1 hour.

* The second video talks about deadlines and their effectiveness, and concludes that deadlines work because of several reasons some of which are:-
1. Deadlines serve as a motivation to work, as one looks to finish the work before the deadline.
2. Deadlines help to regulate breaks as one knows how long they need to work before taking a break.

* The third video talks about the effects of deep work, the video tells that one can practice deep work in day-to-day life. It shows light to the point that deep working develops Myelin around the neurons that help the neurons to fire faster, which improves the brain function.
The video also talks about the strategies to implement deep work some of which are:- 
1. Schedule distractions or breaks.

2. Make deep work a habit or a sinusoidal function of time.
3. Get adequate sleep.

## Question 3

How can you implement the principles in your day-to-day life?

## Answer

The steps you can take to implement deep work in your day-to-day life are:-
* At least practice deep work for 1 hour a day.
* Schedule distractions or breaks.
* Practice deep work regularly and make it a habit.
* Get adequate sleep.

## Question 4

Your key takeaways from the video

## Answer
Some of the takeaways from the videos are:-
* Social Media is not a healthy thing as it wastes too much of one's time.
* Social media is designed to harness your attention and keep you distracted for a while.
* Use of social media is also bad for one's mental health as it leads to a comparison of lives which can end up saddening a person.
