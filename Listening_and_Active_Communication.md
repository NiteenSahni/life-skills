# Listening and Active Communication


Q1.) What are the steps/strategies to do Active Listening?


Ans )

There are five key elements of active listening. They all help you ensure that you hear the other
person, and that the other person knows you are listening to what they say.
* Pay attention.
Give the speaker your undivided attention, and acknowledge the message. Recognize that non-
verbal communication also "speaks" loudly.
Look at the speaker directly.
Put aside distracting thoughts.
"Listen" to the speaker's body language.
Refrain from side conversations when listening in a group setting.
* Show that you are listening.
Use your own body language and gestures to convey your attention.
Nod occasionally.
Smile and use other facial expressions.
Note your posture and make sure it is open and inviting.
Encourage the speaker to continue with small verbal comments like “Yes”.
* Read the body language of the speaker.

* Provide feedback. Provide good feedback to the speaker and ask questions if you have any
* Defer judgment. Don't make a judgement until the speaker is done with their point.
* Respond Appropriately.
Active listening is a model for respect and understanding. You are gaining information and
perspective. You add nothing by attacking the speaker or otherwise putting him or her down.
Be candid, open, and honest in your response.
Assert your opinions respectfully.


Q2. ) According to Fisher's model, what are the key points of Reflective Listening? 

Ans ) Dalmar Fisher gave a model for Reflective Communication that talks about the listed elements:-

*  Avoidig the distraction and focusing on the conversation.

* Having a non-judgemental perspective to the speakers point, the listner can embrace the speakers even if they do not readily relate to it.

* The listner should try to mirror the mood of the speaker.

* Summarizing the content of the speaker.

* Responding to the speaker's specific point, without digressing to other subjects.



Q.3 )What are the obstacles in your listening process?

Ans )

Summarizing the obstacles in my listening process:-

* Get distracted by my own train thoughts.

* Don't tend to take notes.

Q.4 ) 
     What can you do to improve your listening?

Ans ) 

* Will try to not get distracted by my own thoughts.

* Start a habit of taking notes.



Q.5 )
 When do you switch to Passive communication style in your day to day life?

Ans )

Instances when i switch to passive speech :-


* When my friends ask for something and it does not agrees with me.



Q.6 )When do you switch into Aggressive communication styles in your day to day life?

Ans )

* When someone annoys me during my work.

* When i raise a valid point and it gets ignored.



Q.7 ) When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?

Ans ) 

* Generally, I, switch to passive agressive/ sarcasm when I am with my friends.



Q.8 ) How can you make your communication assertive? You can analyse the videos and then think what steps you can apply in your own life?

Ans ) Following steps can be taken to make the communication assertive :-

* Choose the right time, one should choose the right time to bring up an issue.

* Be direct. Be direct in your conversation, do not beat around the bush.

*  Say “I,” not “we.”

* Use body language to emphasize your words.

* Stand up for yourself. If you are not wrong, do not doubt yourself and beat yourself down.